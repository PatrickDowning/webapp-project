#### Python Setup
1. Clone repository by running `git clone <ssh/https url>`
2. Go into the root of the project by running `cd storage_website`
3. Run `py -m venv env` from Terminal or CMD window (if you're running a unix based system run `python -m venv env`)
   - **Windows Users**: Run `.\env\Scripts\activate`
   - **Mac Users**: Run `source ./env/bin/activate`
   
   This will create a virtual environment to separate the packages that our application requires from global packages.
4. Run `pip install -r requirements.txt` this will install all the packages required by our project

#### Run server
change database information in app.py: `username`, `password`, and `database` 

enter `py app.py`

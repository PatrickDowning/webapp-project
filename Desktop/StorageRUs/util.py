import psycopg2
from psycopg2 import Error


def connect_to_db(username='postgres', password='7535', host='127.0.0.1', port='5432', database='webapp'):
    try:
        connection = psycopg2.connect(user=username,
                                      password=password,
                                      host=host,
                                      port=port,
                                      database=database)
        cursor = connection.cursor()

        return cursor, connection

    except (Exception, Error) as error:
        print("Error while connecting to PostgreSQL", error)


def disconnect_from_db(connection, cursor):
    if (connection):
        cursor.close()
        connection.close()
    else:
        print("Connection does not work.")

def run_and_fetch_sql(cursor, sql_string=""):
    try:
        cursor.execute(sql_string)
        record = cursor.fetchall()
        return record
    except (Exception, Error) as error:
        print("Errors while executes the code: ", error)
        return -1

def runSQL(cursor, sql_string=""):
    try:
        cursor.execute(sql_string)
        return 1
    except (Exception, Error) as error:
        print("Errors while executes the code: ", error)
        return -1

def runnerSQL(cursor, sql_string=""):
    cursor.execute(sql_string)
    return

class DatabaseConnection:
    def __init__(self, username, password, host, port, database):
        self.username = username
        self.password = password
        self.host = host
        self.port = port
        self.database = database
        print(self.username)
        self.connection = psycopg2.connect(user=self.username,
                                           password=self.password,
                                           host=self.host,
                                           port=self.port,
                                           database=self.database)
    
    def setup_database(self):
        print("============================")
        
        with self.getCursor() as cur:
            cur.execute("""
                SELECT table_name FROM information_schema.tables
                WHERE table_schema = 'public'
                """)
            records = cur.fetchall()
            existingTables = [t[0] for t in records]
            print (existingTables)
            if "users" not in existingTables: 
                cur.execute("""
                    CREATE TABLE users (
                        id serial PRIMARY KEY,
                        username varchar(255) UNIQUE NOT NULL,
                        password varchar(255) NOT NULL
                    )
                """)
            if "item" not in existingTables:
                cur.execute("""
                    CREATE TABLE item(
                        id serial PRIMARY KEY,
                        username varchar(255),
                        serial_Num varchar(255),
                        item_quantity int,
                        item_name varchar(255)
                    )
                """)
            self.connection.commit()
           
        print("============================")

    def getCursor(self):
        return self.connection.cursor()

    def runSQL(self, sql_string):
        try:
            with self.getCursor() as cur:
                cur.execute(sql_string)
                return True
        except Exception as e:
            print("Errors while executes the code: ", e)
            return False

    def runAndCommitSQL(self, sql_string):
        try:
            with self.getCursor() as cur:
                cur.execute(sql_string)
                self.connection.commit()
                return True
        except Exception as e:
            print("Errors while executes the code: ", e)
            return False

    def runAndFetchSQL(self, sql_string):
        try:
            with self.getCursor() as cur:
                cur.execute(sql_string)
                record = cur.fetchall()
                return (record, cur.description)
        except (Exception, Error) as error:
            print("Errors while executes the code: ", error)
            return None
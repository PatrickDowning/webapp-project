from asyncio.windows_events import NULL
from flask import Flask, render_template, request, url_for, session, flash, redirect
from werkzeug.utils import redirect
from dotenv import load_dotenv
from werkzeug.exceptions import HTTPException
import functools
import os


import util

app = Flask(__name__)

username = 'postgres'
password = '7535'
host = '127.0.0.1'
port = '5432'
database = 'webapp'

db = util.DatabaseConnection(username, password, host, port, database)

db.setup_database()

@app.route('/')
def index():
    return render_template('index.html')


@app.route('/inventory', methods=('GET', 'POST'))
def inventory():
    if request.method == 'POST':
        if request.form['invName'] == "add":
            sNum = request.form['sNum']
            name = request.form['name']
            iQuantity = request.form['iQuantity']
            if session.get("username") is None:
                flash('Sign in to add inventory')
            else:
                records, _ = db.runAndFetchSQL(f"""
                    SELECT serial_Num FROM item
                    WHERE serial_Num='{sNum}' AND username='{session['username']}';
                    """)
                if len(records) < 1:
                    qry = db.runAndCommitSQL(f"""
                        INSERT INTO item (
                            serial_Num,
                            username,
                            item_quantity,
                            item_name
                        )
                        VALUES (
                            '{sNum}',
                            '{session['username']}',
                            {iQuantity},
                            '{name}'
                        );
                        """)
                    return redirect(url_for('inventory'))
                else:
                    flash("Serial Number " + sNum + " already exists in the database")

        else:
            fNum = request.form['fNum']
            if session.get("username") is None:
                flash('Sign in to search inventory')
            else:
                records, description = db.runAndFetchSQL(f"""
                    SELECT item_quantity FROM item
                    WHERE serial_Num='{fNum}' AND username='{session['username']}';
                    """)
                if len(records) < 1:
                    flash('Serial number not found')

                else:
                    tup =list(zip(*records))[0]

                    quantity = tup[0]

                    if quantity == 1:
                        flash("There is " + str(quantity) + " item with Serial Number " + fNum)
                    else:
                        flash("There is " + str(quantity) + " items with Serial Number " + fNum)
                    
        
    return render_template('inventory.html')


@app.route('/settings', methods=('GET', 'POST'))
def settings():
    if request.method == 'POST':
        if request.form['invName'] == "edit":
            sNum = request.form['num']

            if session.get("username") is None:
                flash('Sign in to edit inventory')
            else:
                records, description = db.runAndFetchSQL(f"""
                    SELECT serial_Num FROM item
                    WHERE serial_Num='{sNum}' AND username='{session['username']}';
                    """)

                if len(records) < 1:
                    flash('Serial Number ' + sNum + ' not found')
                else:
                    session['serial'] = sNum
                    return redirect(url_for('edit'))

        else:
            fNum = request.form['fNum']
            iQuantity = request.form['iQuantity']

            if session.get("username") is None:
                flash('Sign in to remove inventory')
            else:
                records, description = db.runAndFetchSQL(f"""
                    SELECT item_quantity FROM item
                    WHERE serial_Num='{fNum}' AND username='{session['username']}';
                    """)
                if len(records) < 1:
                    flash('Serial number not found')
                
                else:
                    tup =list(zip(*records))[0]

                    quantity = tup[0]
                    
                    iQ = int(iQuantity)
                    if iQ > quantity:
                        flash('Cannot remove more than currently in inventory')
                    elif quantity == iQ:
                        qry = db.runAndCommitSQL(f"""
                            DELETE FROM item 
                            WHERE serial_Num = '{fNum}' AND username='{session['username']}';
                            """)
                        flash('Item with serial number ' + fNum + ' removed')
                        return redirect(url_for('settings'))
                    else:
                        nQ = quantity-iQ
                        qry = db.runAndCommitSQL(f"""
                            UPDATE item
                            SET item_quantity = {nQ}
                            WHERE serial_Num = '{fNum}' AND username='{session['username']}';
                            """)
                        flash('There is now ' + str(nQ) + ' items with serial number ' + fNum)
                        return redirect(url_for('settings'))
                    
    return render_template('settings.html')

@app.route('/edit', methods=["GET", 'POST'])
def edit():
    
    if request.method == 'POST':
            
        sNum = request.form['sNum']
        name = request.form['name']
        iQuantity = request.form['iQuantity']
        
        if name:
            qry = db.runAndCommitSQL(f"""
                        UPDATE item
                        SET item_name = '{name}'
                        WHERE serial_Num = '{session['serial']}' AND username='{session['username']}';
                        """)
            if iQuantity:
                iQ = int(iQuantity)
                qry = db.runAndCommitSQL(f"""
                            UPDATE item
                            SET item_quantity = {iQ}
                            WHERE serial_Num = '{session['serial']}' AND username='{session['username']}';
                            """)
                if sNum:
                    records, _ = db.runAndFetchSQL(f"""
                            SELECT serial_Num FROM item
                            WHERE serial_Num='{sNum}' AND username='{session['username']}';
                            """)
                    if len(records) < 1:
                        qry = db.runAndCommitSQL(f"""
                                    UPDATE item
                                    SET serial_Num = '{sNum}'
                                    WHERE serial_Num = '{session['serial']}' AND username='{session['username']}';
                                    """)
            return redirect(url_for('index'))
            
        if iQuantity:
            iQ = int(iQuantity)
            qry = db.runAndCommitSQL(f"""
                        UPDATE item
                        SET item_quantity = {iQ}
                        WHERE serial_Num = '{session['serial']}' AND username='{session['username']}';
                        """)
            if sNum:
                records, _ = db.runAndFetchSQL(f"""
                        SELECT serial_Num FROM item
                        WHERE serial_Num='{sNum}' AND username='{session['username']}';
                        """)
                if len(records) < 1:
                    qry = db.runAndCommitSQL(f"""
                                UPDATE item
                                SET serial_Num = '{sNum}'
                                WHERE serial_Num = '{session['serial']}' AND username='{session['username']}';
                                """)
            return redirect(url_for('index'))

        if sNum:
            records, _ = db.runAndFetchSQL(f"""
                    SELECT serial_Num FROM item
                    WHERE serial_Num='{sNum}' AND username='{session['username']}';
                    """)
            if len(records) < 1:
                qry = db.runAndCommitSQL(f"""
                            UPDATE item
                            SET serial_Num = '{sNum}'
                            WHERE serial_Num = '{session['serial']}' AND username='{session['username']}';
                            """)
                if not name:
                    return redirect(url_for('index'))
            else:
                flash("Serial Number " + sNum + " already exists in the database")
            if name:
                qry = db.runAndCommitSQL(f"""
                            UPDATE item
                            SET item_name = '{name}'
                            WHERE serial_Num = '{session['serial']}' AND username='{session['username']}';
                            """)
                return redirect(url_for('index'))

    else:
        flash('editing inventory with serial number ' + session['serial'])
    return render_template('edit.html')

@app.route('/login', methods=["GET", 'POST'])
def signIn():
    if request.method == 'POST':
        username = request.form.get("email")
        password = request.form.get("password")
        print("username and password:")
        print(username + ' ' + password)
        records, _ = db.runAndFetchSQL(f"""
            SELECT username FROM users
            WHERE username='{username}' and password='{password}';
            """)
        if len(records) == 0:
            return redirect(url_for('fail'))
        else:
            session['username'] = username
            return redirect(url_for('index'))
    return render_template('signIn.html')

@app.route('/logout')
def logout():
    session.pop('username')
    return render_template('index.html')

@app.route('/signUp', methods=["GET", 'POST'])
def signUp():
    if request.method == "POST":
        username = request.form.get("email")
        password = request.form.get("psw")
        print(username + ' ' + password)
        records, _ = db.runAndFetchSQL(f"""
            SELECT username FROM users
            WHERE username='{username}';
            """)
        if len(records) < 1:
            qry = db.runAndCommitSQL(f"""
                INSERT INTO users (
                    username,
                    password
                )
                VALUES (
                    '{username}',
                    '{password}'
                );
                """)
            session['username'] = username
            return redirect(url_for('index'))
        else:
            print(f"User {username} already exists in the database!!!")
            return redirect(url_for('fail'))
    return render_template('signUp.html')

@app.route('/userPageFail')
def fail():
    return render_template('userPageFail.html')


if __name__ == '__main__':
    # set debug mode
    app.debug = True
    app.secret_key = "banana"
    app.run()